{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "91ea1706",
   "metadata": {},
   "source": [
    "# Selecting frequencies"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "trained-advance",
   "metadata": {},
   "source": [
    "## Preliminaries"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "otherwise-egypt",
   "metadata": {},
   "source": [
    "We load here some common utilities that we use all the time in cryo-EM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "arabic-falls",
   "metadata": {},
   "outputs": [],
   "source": [
    "%run ../cryoem-common/cryoem_common.ipynb\n",
    "add_latex_commands()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "destroyed-phrase",
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.signal import convolve, windows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dependent-karen",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot2(a1, a2, title1='', title2=''):\n",
    "    \"Put two sets of plots next to each other\"\n",
    "    plt.figure(figsize=(8, 2))\n",
    "    plt.subplot(121)\n",
    "    for ai in a1:\n",
    "        plt.plot(ai, alpha=1/len(a1))\n",
    "    plt.title(title1)\n",
    "\n",
    "    plt.subplot(122)\n",
    "    for ai in a2:\n",
    "        plt.plot(ai, alpha=1/len(a2))\n",
    "    plt.title(title2);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b236f50",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6a7969b",
   "metadata": {},
   "source": [
    "How do we \"select frequencies\"?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e275a79c",
   "metadata": {},
   "source": [
    "Let's have an array `ar` with `n` real values, and `af = fft(ar)`. Then, the following relations are true to within numerical accuracy:\n",
    "\n",
    "* `ifft(af) == ar`\n",
    "* `sum(abs(af)**2) == n * sum(ar**2)`\n",
    "* `af[0] == sum(ar)` (note that the 0th bin corresponds to frequency 0)\n",
    "* `af[1:] = 0` $\\to$ `ifft(af) == 1/n * sum(ar)` (an array with all values equal to the mean of `ar`)\n",
    "\n",
    "If `pfs = [pf0, pf1, ...]` is a [partition of unity](https://en.wikipedia.org/wiki/Partition_of_unity) (in frequency space), then:\n",
    "\n",
    "* `ar == sum(ifft(af * pfi) for pfi in pfs)`\n",
    "\n",
    "(note that if we use numpy's `sum`, we would write it like `sum(np.array(...), axis=0)`).\n",
    "\n",
    "This can be considered a **decomposition of `ar` in frequencies**, each one being `ifft(af * pfi)`.\n",
    "\n",
    "Since `(i)fft`s of products are convolutions of the `(i)fft`s (and viceversa), $\\IF{\\hat{f} \\cdot \\hat{g}} = \\IF{\\hat{f}} * \\IF{\\hat{g}}$, we can see each of the terms `ifft(af * pfi)` in the decomposition in frequencies as\n",
    "`convolve(ar, pri, mode='same')`, with `pri = ifftshift(ifft(pfi))` (the `ifftshift` is needed due to [`convolve`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.convolve.html) expecting the signal to be centered).\n",
    "\n",
    "If we take the `pfi` not only real, but also symmetric (functions with $p_f(-f) = p_f(f)$), which we normally would to select \"a frequency\" (treating the same all the points at a certain absolute frequency), then `ifft(pfi)` is real and symmetric too:\n",
    "\n",
    "$$\n",
    "p_r^*(x) = \\left(\\int \\!d\\!f \\, e^{2 \\pi i f x} p_f(f)\\right)^* = \\int \\!d\\!f \\, e^{- 2 \\pi i f x} p_f^*(f) = \\int \\!d\\!f \\, e^{- 2 \\pi i f x} p_f(f) = \\int \\!d\\!f' \\, e^{2 \\pi i f' x} p_f(-f') = \\int \\!d\\!f' \\, e^{2 \\pi i f' x} p_f(f') = p_r(x) \\\\\n",
    "p_r(-x) = \\int \\!d\\!f \\, e^{2 \\pi i f (-x)} p_f(f) = \\int \\!d\\!f' \\, e^{2 \\pi i f' x} p_f(-f') = \\int \\!d\\!f' \\, e^{2 \\pi i f' x} p_f(f') = p_r(x)\n",
    "$$\n",
    "\n",
    "Moreover, the convolution with a symmetric function is easy to visualize, since:\n",
    "\n",
    "$$\n",
    "a_r * p_r (x) = \\int \\!dx' a_r(x') \\, p_r(x - x') = \\int \\!dx' a_r(x') \\, p_r(x' - x)\n",
    "$$\n",
    "\n",
    "that is, to find the convolution at each point $x$ we just need to translate $p_r$ so it is centered at $x$, and then see how much it correlates with $a_r$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "566b0984",
   "metadata": {},
   "source": [
    "So, what do the `pri` look like then?\n",
    "\n",
    "Let's look at two examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "feaa6f3b",
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 100\n",
    "ar = np.random.randn(n)\n",
    "af = fft(ar)\n",
    "\n",
    "f = fftfreq(n)\n",
    "\n",
    "pf0 = exp(-f**2/0.002)\n",
    "pr0 = ifftshift(real(ifft(pf0)))\n",
    "\n",
    "pf1 = exp(-f**2/0.02) - pf0\n",
    "pr1 = ifftshift(real(ifft(pf1)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "efacf64a",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2([fftshift(pf0), fftshift(pf1)], [pr0, pr1],\n",
    "      'in frequency space (multiply)', 'in real space (convolve)')\n",
    "\n",
    "ar0 = real(ifft(af * pf0))\n",
    "ar1 = real(ifft(af * pf1))\n",
    "plot2([ar, ar0, ar1], [ar, ar0 + ar1],\n",
    "      'signal and two freq. components', 'signal and sum of freq. components')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4a6c7cd",
   "metadata": {},
   "source": [
    "This example is similar to the use of a [difference of gaussians wavelet](https://en.wikipedia.org/wiki/Difference_of_Gaussians)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "962bb1e3",
   "metadata": {},
   "source": [
    "### Gabor filters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c8f031e",
   "metadata": {},
   "source": [
    "If for our `pfi` we use gaussians like `pfi = exp(-(f_norm - fi)**2 / (2*sigma2))` centered at different frequencies `fi`, then we are basically using the wavelets that appear in [Gabor filters](https://en.wikipedia.org/wiki/Gabor_filter#Wavelet_space)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ea998d8",
   "metadata": {},
   "source": [
    "Those `pfi` would not really form a partition of unity, but depending on our application that may not matter."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8f9c4f9",
   "metadata": {},
   "source": [
    "Since those `pfi` can be seen as the **convolution** of a *gaussian centered at 0* and a *delta at frequency $|f|$*, their corresponding functions in real space would be simply [gaussian wave packets](https://en.wikipedia.org/wiki/Wave_packet) centered at 0, that is, the **product** of a *gaussian whose width only depends on `sigma2`* (the inverse Fourier transform of the original gaussian) and a *cosine whose frequency only depends on `f`* (the inverse Fourier transform of the delta at $|f|$):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f30ddd19",
   "metadata": {},
   "outputs": [],
   "source": [
    "pf0 = exp(-f**2/0.005)\n",
    "pr0 = ifftshift(real(ifft(pf0)))\n",
    "\n",
    "pf1 = exp(-(abs(f) - 0.2)**2/0.005)\n",
    "pr1 = ifftshift(real(ifft(pf1)))\n",
    "\n",
    "pf2 = exp(-(abs(f) - 0.4)**2/0.005)\n",
    "pr2 = ifftshift(real(ifft(pf2)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0442189",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2([fftshift(pf0), fftshift(pf1), fftshift(pf2)], [pr0, pr1, pr2],\n",
    "      'in frequency space (multiply)', 'in real space (convolve)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54e1bc28",
   "metadata": {},
   "source": [
    "### Discrete successive approximations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cde749e1",
   "metadata": {},
   "source": [
    "This is an illustration of an approximation by adding each frequency discretely. It is like the sum of deltas in some sense."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7ad807d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def freq_approx(ar):\n",
    "    \"Yield successive better approximations in frequency to ar\"\n",
    "    active_freqs = np.zeros(len(ar))\n",
    "    af = fft(ar)\n",
    "    for i in range(len(af)//2):\n",
    "        active_freqs[i] = active_freqs[-i] = 1\n",
    "        yield real(ifft(af * active_freqs))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99e7630e",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "apps = list(freq_approx(ar))\n",
    "plot2([ar, apps[3], apps[6]], [ar, apps[10], apps[20]], '3 and 6 approximations', '10 and 20 approximations')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d36be07",
   "metadata": {},
   "source": [
    "## In 2D"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "98e24c97",
   "metadata": {},
   "source": [
    "All the previous discussion is valid for any dimension. In 3D, for example, `n` is the total number of voxels, and we use `(i)fftn` instead of `(i)fft` and so on."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58655c9c",
   "metadata": {},
   "source": [
    " Let's see an example in 2D with an image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fe2b04bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "from PIL import Image\n",
    "def imshow(x): plt.imshow(x, cmap='gray')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9dadd5ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "ar = np.array(Image.open('example_cameraman.png'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8974585",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "imshow(ar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa04a3ba",
   "metadata": {},
   "outputs": [],
   "source": [
    "af = fftn(ar)\n",
    "\n",
    "f = fftfreq(ar.shape[0])\n",
    "fx, fy = meshgrid(f, f)\n",
    "f_norm = sqrt(fx**2 + fy**2)\n",
    "\n",
    "w1 = exp(-f_norm**2/.01)\n",
    "w2 = exp(-f_norm**2/.1) - w1\n",
    "w3 = exp(-f_norm**2/10) - w2 - w1\n",
    "im1, im2, im3 = [real(ifftn(af * wi)) for wi in [w1, w2, w3]]\n",
    "plt.figure(figsize=(9,6))\n",
    "plt.subplot(231);  imshow(fftshift(w1))\n",
    "plt.subplot(232);  imshow(fftshift(w2))\n",
    "plt.subplot(233);  imshow(fftshift(w3))\n",
    "plt.subplot(234);  imshow(im1)\n",
    "plt.subplot(235);  imshow(im2)\n",
    "plt.subplot(236);  imshow(im3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1a25436",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "imshow(im1 + im2 + im3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "august-highway",
   "metadata": {},
   "source": [
    "## In 3D"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "armed-viking",
   "metadata": {},
   "source": [
    "I'd like to mix this with the [notebook on the Guinier plot](https://gitlab.com/jordibc/guinier-plot) and put the theory and examples together."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "everyday-boxing",
   "metadata": {},
   "source": [
    "### Volume and 1D slice"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "tamil-louis",
   "metadata": {},
   "source": [
    "Let's read an example volume and have a look at it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "lyric-motor",
   "metadata": {},
   "outputs": [],
   "source": [
    "V, voxel_n, voxel_size = get_vol_and_voxel('emd_10418_half_map_1.map')\n",
    "plot(V, title='Original volume V')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "practical-referral",
   "metadata": {},
   "source": [
    "If we take a 1-D slice through the center, this is what the volume looks like, and its Fourier tranform."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "north-restriction",
   "metadata": {},
   "outputs": [],
   "source": [
    "V_slice = V[voxel_n//2,:,voxel_n//2]\n",
    "FV_slice = np.fft.fft(V_slice)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "according-middle",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot2([V_slice], [abs(fftshift(FV_slice))],\n",
    "      'V_slice', 'Fourier transform')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "classical-punishment",
   "metadata": {},
   "source": [
    "### 1D Simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fitted-provincial",
   "metadata": {},
   "source": [
    "Let's take `n` \"atoms\" spread randomly (with a uniform distribution). This is the \"bag of atoms\" that appears for example at [the paper about Wilson statistics](https://www.biorxiv.org/content/10.1101/2021.05.14.444177v1). Each atom's Coulomb potential will be taken as a Gaussian. Finally, we add some noise too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "confidential-publication",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_atoms = 15\n",
    "centers = voxel_n * np.random.random(n_atoms)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "advised-opening",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = linspace(0, voxel_n-1, voxel_n)\n",
    "\n",
    "ar = sum(np.array([4*exp(-(x-x0)**2/2) for x0 in centers]), axis=0) + 0.8 * np.random.randn(voxel_n)\n",
    "af = fft(ar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aging-republic",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "plot2([ar], [fftshift(abs(af))],\n",
    "      'signal+noise', 'Fourier transform')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "first-conditions",
   "metadata": {},
   "outputs": [],
   "source": [
    "wr = windows.gaussian(voxel_n, 2)\n",
    "wr /= sum(wr)\n",
    "\n",
    "awr = convolve(ar, wr, mode='same')\n",
    "\n",
    "plot2([wr], [ar, awr],\n",
    "      'window', 'signal and filtered signal (convolution)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unable-cathedral",
   "metadata": {},
   "source": [
    "Doing it \"manually\" in the Fourier domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "harmful-regular",
   "metadata": {},
   "outputs": [],
   "source": [
    "wf = fft(ifftshift(wr))\n",
    "\n",
    "plot2([fftshift(real(wf))], [ar, real(ifft(af * wf))],\n",
    "     'window in the Fourier domain', 'signal and filtered signal (Fourier)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08801230",
   "metadata": {},
   "source": [
    "### Back to the original volume"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d31274d",
   "metadata": {},
   "source": [
    "We can see how the methods apply to the volume slice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "magnetic-cookie",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "Vw = convolve(V_slice, wr, mode='same')\n",
    "plot2([wr], [V_slice, Vw],\n",
    "      'window', 'V slice and filtered')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "hollow-scoop",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get ready to select frequencies (using a shell in frequency space).\n",
    "fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "f_norm = sqrt(fx**2 + fy**2 + fz**2)\n",
    "\n",
    "f_voxel_width = 4.6  # width (in voxels) of the shell\n",
    "f_width = f_voxel_width / (voxel_n * voxel_size)  # frequency width\n",
    "\n",
    "def shell(f):\n",
    "    \"Return a shell of spatial frequencies, around frequency f\"\n",
    "    return exp(- (f_norm - f)**2 / (2 * f_width**2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "southeast-overview",
   "metadata": {},
   "outputs": [],
   "source": [
    "FV = fftn(V)\n",
    "S = shell(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fdb686cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "fplot(S)\n",
    "plot(real(ifftn(FV * S)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "interpreted-silly",
   "metadata": {},
   "outputs": [],
   "source": [
    "#for f in [0.2, 0.3, 0.5]:\n",
    "#    S = shell(f)\n",
    "#    fplot(S)\n",
    "#    plot(real(ifftn(FV * S)))\n",
    "\n",
    "f_width /= 4  # tests\n",
    "f = 0.2\n",
    "S = shell(f)\n",
    "fplot(S)\n",
    "plot(real(ifftshift(ifftn(S))))\n",
    "plot(real(ifftn(FV * S)))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
